'use strict'
class RectangleNode  {
    constructor(x, y, width, color){
        this.x = x
        this.y = y
        this.width = width
        this.color = color
        this.height = 45

        this.text1 = undefined
        this.text2 = undefined
        this.text3 = undefined
    }
    
    clone(){
        return new RectangleNode(this.x, this.y, this.width, this.color)
    }

    setText1(text){ this.text1 = text }

    getText1(){ return this.text1 }

    setText2(text){ this.text2 = text }

    getText2(){ return this.text2 }

    setText3(text){ this.text3 = text }

    getText3(){ return this.text3 }

    setWidth(w){ this.width = w }

    getWidth(){ return this.width }

    setHeight(h){ this.height = h }

    getHeight(){ return this.height }

    getBounds(){
        return {
            x: this.x, 
            y: this.y,  
            width: this.width, 
            height: this.height 
        } 
    }

    contains(p){
        return (p.x >= this.x && p.x <= this.x + this.width) && (p.y >= this.y && p.y <= this.y + this.height)
    }

    translate(dx, dy){
        this.x += dx
        this.y += dy
    }

    draw(){
        const panel = document.getElementById('graphpanel')
        const classNode = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
        classNode.setAttribute('x', this.x)
        classNode.setAttribute('y', this.y)
        classNode.setAttribute('width', this.width)
        classNode.setAttribute('height', this.height)
        classNode.setAttribute('style', 'stroke: black; stroke-width:1; ')
        classNode.setAttribute('fill', this.color)
        panel.appendChild(classNode)
      }
    
    getConnectionPoint(other){
        let centerX = this.x + this.width / 2;
        let centerY = this.y + this.height / 2;
        let dx = other.x - centerX;
        let dy = other.y - centerY;
        let slope = getBounds().height / getBounds().width

        if (dx !== 0 && -slope <= dy / dx && dy / dx <= slope)
        {
            if (dx > 0){
                centerX = getBounds().x + getBounds().width,
                centerY += (getBounds().width /2) * dy / dx
            }
            else{       
                centerX = getBounds().x,
                centerY -= (getBounds().width /2) * dy / dx
            }
        }
        else if (dy !== 0){
            if (dy > 0){
                centerX += (getBounds().height / 2) * dx / dy,
                centerY = getBounds().y + getBounds().height
            }
            else {
                centerX -= (getBounds().height / 2) * dx / dy,
                centerY = getBounds().y
            }
        }

        return{
            x: centerX,
            y: centerY
        }
      
    }
      
}

class ClassNode extends RectangleNode{
    constructor(x, y, width, color){
        super (x, y, width, color)
        this.midHeight = 20
        this.botHeight = 20
        //setBounds ({x: 0, y:0, width: width, height: height})
    }

    clone(){
        return new ClassNode(this.x, this.y, this.width, this.color)
    }

    

    getproperties(){
        return [
        {name: 'name', value: this.text1},
        {name: 'attributes', value: this.text2},
        {name: 'methods', value: this.text3},
        ]
        }


    draw(){
        const padding = 10
        const spacing = 5
        const fontSize = 15
        const topHeight = 45 // top's height doesn't change if there is a text inside or not, it's always same height
        let midHasText = false
        let botHasText = false
        let textTop = {textContent: this.text1, width: this.width, height: 40}
        let textMid = {textContent: this.text2, width: this.width, height : 0}
        let textBot = {textContent: this.text3, width: this.width, height : 0}
        
        const panel = document.getElementById('graphpanel')
        super.draw() 

        // draw the top box with text
        if (textTop.textContent !== undefined){
            const top = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
            top.setAttribute('x', this.x)
            top.setAttribute('y', this.y)
            top.setAttribute('width', this.width)
            top.setAttribute('height', topHeight)
            top.setAttribute('fill', this.color)
            top.setAttribute('stroke', 'black')
            panel.appendChild(top)

            const name = document.createElementNS('http://www.w3.org/2000/svg', 'text')
            name.setAttribute('x', this.x + padding)
            name.setAttribute('y', this.y + padding * 3)
            name.setAttribute('font-size', 20)
            name.textContent = textTop.textContent
            panel.appendChild(name)
            textTop.width = name.getBBox().width + padding * 2
        }
        
        // draw the middle box with text
        if (textMid.textContent !== undefined){
            midHasText = true
            const mid = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
            mid.setAttribute('x', this.x)
            mid.setAttribute('y', this.y + topHeight)
            mid.setAttribute('width', this.width)
            mid.setAttribute('height', this.midHeight)
            mid.setAttribute('fill', 'none')
            mid.setAttribute('stroke', 'black')
            panel.appendChild(mid)

            const attributes = document.createElementNS('http://www.w3.org/2000/svg', 'text')
            attributes.setAttribute('x', this.x + padding)
            attributes.setAttribute('y', this.y + topHeight + fontSize + spacing)
            attributes.setAttribute('font-size', fontSize)
            attributes.textContent = textMid.textContent
            panel.appendChild(attributes)

            textMid.width = attributes.getBBox().width + padding * 2
            textMid.height = attributes.getBBox().height + spacing * 2
        }

        // draw the bottom box with text
        if (textBot.textContent !== undefined){
            botHasText = true
            const bot = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
            bot.setAttribute('x', this.x)
            bot.setAttribute('y', midHasText ? this.y + topHeight + this.midHeight : this.y + topHeight)
            bot.setAttribute('width', this.width)
            bot.setAttribute('height', this.botHeight)
            bot.setAttribute('fill', 'none')
            bot.setAttribute('stroke', 'black')
            panel.appendChild(bot)

            const methods = document.createElementNS('http://www.w3.org/2000/svg', 'text')
            methods.setAttribute('x', this.x + padding)
            methods.setAttribute('y', midHasText ? this.y + topHeight + this.midHeight + fontSize + spacing: this.y + topHeight + fontSize + spacing)
            methods.setAttribute('font-size', fontSize)
            methods.textContent = textBot.textContent
            panel.appendChild(methods)

            textBot.width = methods.getBBox().width + padding * 2
            textBot.height = methods.getBBox().height + spacing * 2
        }
        // change width of classNode if any text is runing out of space
        this.width = Math.max(textTop.width, Math.max(textMid.width, Math.max(textBot.width, this.width)))

        // change height of classNode
        if ( this.midHeight < textMid.height)
                this.midHeight = textMid.height
        if ( this.botHeight < textBot.height)
                this.botHeight = textBot.height
        
        if (!midHasText && !botHasText)
            this.height = topHeight
        else if (midHasText && botHasText)
            this.height = topHeight + this.midHeight + this.botHeight
        else if (midHasText)
            this.height = topHeight + this.midHeight
        else if (botHasText)
            this.height = topHeight + this.botHeight
    
    }
    
}

class InterfaceNode extends RectangleNode{
    constructor(x, y, width, color){
        super (x, y, width, color)
        this.botHeight = 20
        //setBounds ({x: 0, y:0, width: width, height: height})
    }

    clone(){
        return new InterfaceNode(this.x, this.y, this.width, this.color)
    }

    getproperties(){
        return [
        {name: 'name', value: this.text1},
        {name: 'methods', value: this.text2},
        ]
    }

    draw(){
        const padding = 10
        const spacing = 5
        const fontSize = 15
        const topHeight = 45 // top's height doesn't change if there is a text inside or not, it's always same height
        let botHasText = false
        let textTop = {textContent: this.text1, width: this.width, height: topHeight}
        let textBot = {textContent: this.text2, width: this.width, height : 0}
        
        const panel = document.getElementById('graphpanel')
        super.draw() 
        // draw <<interface>> headline
            let head = document.createElementNS('http://www.w3.org/2000/svg', 'text')
            head.setAttribute('x', this.x + 10)
            head.setAttribute('y', this.y + 20 )
            head.setAttribute('font-size', fontSize)
            head.textContent = '<<interface>>'
            panel.appendChild(head)
            
        // draw the top box with text
        if (textTop.textContent !== undefined){
            const name = document.createElementNS('http://www.w3.org/2000/svg', 'text')
            name.setAttribute('x', this.x + padding)
            name.setAttribute('y', this.y + padding * 3 + 5)
            name.setAttribute('font-size', 20)
            name.textContent = textTop.textContent
            panel.appendChild(name)
            textTop.width = name.getBBox().width + padding * 2

            // repaint headline
            head.setAttribute('x', textTop.width < 100 ? this.x + 10 : this.x + name.getBBox().width / 2 - 40)
            head.setAttribute('y', this.y + 15 )
            head.setAttribute('font-size', fontSize - 2)
            head.textContent = '<<interface>>'
            panel.appendChild(head)
        }

        // draw the bottom box with text
        if (textBot.textContent !== undefined){
            botHasText = true
            const bot = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
            bot.setAttribute('x', this.x)
            bot.setAttribute('y', this.y + topHeight)
            bot.setAttribute('width', this.width)
            bot.setAttribute('height', this.botHeight)
            bot.setAttribute('fill', 'none')
            bot.setAttribute('stroke', 'black')
            panel.appendChild(bot)

            const methods = document.createElementNS('http://www.w3.org/2000/svg', 'text')
            methods.setAttribute('x', this.x + padding)
            methods.setAttribute('y', this.y + topHeight + fontSize + spacing)
            methods.setAttribute('font-size', fontSize)
            methods.textContent = textBot.textContent
            panel.appendChild(methods)

            textBot.width = methods.getBBox().width + padding * 2
            textBot.height = methods.getBBox().height + spacing * 2
        }
        // change width of classNode if any text is runing out of space
        this.width = Math.max(textTop.width, Math.max(textBot.width, this.width))

        // change height of classNode
        if ( this.botHeight < textBot.height)
                this.botHeight = textBot.height

        this.height = botHasText ? topHeight + this.botHeight : topHeight
    }
    
}

class EnumNode extends RectangleNode{
    constructor(x, y, width, color){
        super (x, y, width, color)
        this.botHeight = 20
        //setBounds ({x: 0, y:0, width: width, height: height})
    }

    clone(){
        return new EnumNode(this.x, this.y, this.width, this.color)
    }

    getproperties(){
        return [
        {name: 'name', value: this.text1},
        ]
        }

    draw(){
        const padding = 10
        const spacing = 5
        const fontSize = 15
        const topHeight = 45 // top's height doesn't change if there is a text inside or not, it's always same height
        let textTop = {textContent: this.text1, width: this.width, height: topHeight}
        
        const panel = document.getElementById('graphpanel')
        super.draw() 
        // draw <<interface>> headline
            let head = document.createElementNS('http://www.w3.org/2000/svg', 'text')
            head.setAttribute('x', this.x + 5)
            head.setAttribute('y', this.y + 20 )
            head.setAttribute('font-size', fontSize - 2)
            head.textContent = '<<enumeration>>'
            panel.appendChild(head)
            
        // draw the top box with text
        if (textTop.textContent !== undefined){
            
            const name = document.createElementNS('http://www.w3.org/2000/svg', 'text')
            name.setAttribute('x', this.x + padding)
            name.setAttribute('y', this.y + padding * 3 + 5)
            name.setAttribute('font-size', 20)
            name.textContent = textTop.textContent
            panel.appendChild(name)
            textTop.width = name.getBBox().width + padding * 2

            // repaint headline
            head.setAttribute('x', textTop.width < 100 ? this.x + 10 : this.x + name.getBBox().width / 2 - 40)
            head.setAttribute('y', this.y + 15 )
            head.setAttribute('font-size', fontSize - 2)
            head.textContent = '<<enumeration>>'
            panel.appendChild(head)
        }
        // change width of classNode if any text is runing out of space
        this.width = Math.max(textTop.width, this.width)     
    }
    
}

class PackageNode extends RectangleNode{
    constructor(x, y, width, color){
        super (x, y, width, color)
        this.topWidth = 60
        this.botHeight = 130
    }

    clone(){
        return new PackageNode(this.x, this.y, this.width, this.color)
    }

    getproperties(){
        return [
        {name: 'Package Name', value: this.text1},
        {name: 'Text', value: this.text2},
        ]
        }


    draw(){
        const padding = 10
        const spacing = 5
        const fontSize = 15
        const topHeight = 30 // top's height doesn't change if there is a text inside or not, it's always same height
        
        let textTop = {textContent: this.text1, width: 0, height: 20}
        let textBot = {textContent: this.text2, width: 0, height : 0}
        
        const panel = document.getElementById('graphpanel')
        const top = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
        top.setAttribute('x', this.x)
        top.setAttribute('y', this.y)
        top.setAttribute('width', this.topWidth)
        top.setAttribute('height', topHeight)
        top.setAttribute('fill', this.color)
        top.setAttribute('stroke', 'black')
        panel.appendChild(top)

        const bot = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
        bot.setAttribute('x', this.x)
        bot.setAttribute('y', this.y + topHeight)
        bot.setAttribute('width', this.width)
        bot.setAttribute('height', this.botHeight)
        bot.setAttribute('fill', this.color)
        bot.setAttribute('stroke', 'black')
        panel.appendChild(bot)

        // draw the top box with text
        if (textTop.textContent !== undefined){
            const name = document.createElementNS('http://www.w3.org/2000/svg', 'text')
            name.setAttribute('x', this.x + padding)
            name.setAttribute('y', this.y + fontSize + spacing)
            name.setAttribute('font-size', 20)
            name.textContent = textTop.textContent
            panel.appendChild(name)
            textTop.width = name.getBBox().width + padding * 2
            if (this.topWidth < textTop.width)
                this.topWidth = textTop.width
        }

        // draw the bottom box with text
        if (textBot.textContent !== undefined){
            const methods = document.createElementNS('http://www.w3.org/2000/svg', 'text')
            methods.setAttribute('x', this.x + padding)
            methods.setAttribute('y', this.y + topHeight + fontSize + spacing)
            methods.setAttribute('font-size', fontSize)
            methods.textContent = textBot.textContent
            panel.appendChild(methods)

            textBot.width = methods.getBBox().width + padding * 2
            textBot.height = methods.getBBox().height + spacing * 2
        }
        // change width of classNode if any text is runing out of space
        this.width = Math.max(textTop.width + padding * 4, Math.max(textBot.width, this.width))

        // change height of classNode
        
        if ( this.botHeight < textBot.height)
                this.botHeight = textBot.heigh;
        this.height = topHeight + this.botHeight
    }
    
}


class NoteNode extends RectangleNode{
    constructor(x, y, width, color){
        super (x, y, width, color)
        this.botHeight = 20
    }

    clone(){
        return new NoteNode(this.x, this.y, this.width, this.color)
    }

    getproperties(){
        return [
        {name: 'name', value: this.text1},
        ]
    }

    draw(){
        const padding = 10
        const spacing = 5
        const fontSize = 15
        const topHeight = 45 // top's height doesn't change if there is a text inside or not, it's always same height
        let textTop = {textContent: this.text1, width: this.width, height: topHeight}
        const panel = document.getElementById('graphpanel')
        super.draw() 
        // draw Note symbol
        const top = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
        top.setAttribute('x', this.x + this.width - padding)
        top.setAttribute('y', this.y)
        top.setAttribute('width', padding)
        top.setAttribute('height', padding)
        top.setAttribute('fill', 'white')
        top.setAttribute('stroke', 'black')
        panel.appendChild(top)

        const line = document.createElementNS('http://www.w3.org/2000/svg', 'line')
        line.setAttribute('x1', this.x + this.width - padding)
        line.setAttribute('y1', this.y)
        line.setAttribute('x2', this.x + this.width)
        line.setAttribute('y2', this.y + padding)
        line.setAttribute('fill', 'none')
        line.setAttribute('stroke', 'black')
        panel.appendChild(line)
       
        // draw the top box with text
        if (textTop.textContent !== undefined){
            
            const name = document.createElementNS('http://www.w3.org/2000/svg', 'text')
            name.setAttribute('x', this.x + padding)
            name.setAttribute('y', this.y + padding * 3 + 5)
            name.setAttribute('font-size', 20)
            name.textContent = textTop.textContent
            panel.appendChild(name)
            textTop.width = name.getBBox().width + padding * 2
        }
        // change width of classNode if any text is runing out of space
        this.width = Math.max(textTop.width, this.width)     
    }
    
}