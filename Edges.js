
class Edge{
  constructor(s,e){
    this.start = s
    this.end = e
    let lineStlye = undefined
  }
  clone(){
  return new Edge()
   }

   getBounds(){
     return {
      x: this.start.getBounds().x + this.start.getBounds().width/2,
      y: this.start.getBounds().y + this.start.getBounds().height/2
     }
   }

  getConnectionPoints(){
    let centerX = this.x + this.width / 2;
    let centerY = this.y + this.height / 2;

    let dx = other.x - centerX;
    let dy = other.y - centerY;
    let slope = getBounds().height / getBounds().width

    if (dx !== 0 && -slope <= dy / dx && dy / dx <= slope)
    {
        if (dx > 0){
            centerX = getBounds().x + getBounds().width,
            centerY += (getBounds().width /2) * dy / dx
        }
        else{
            centerX = getBounds().x,
            centerY -= (getBounds().width /2) * dy / dx
        }
    }
    else if (dy !== 0){
        if (dy > 0){
            centerX += (getBounds().height / 2) * dx / dy,
            centerY = getBounds().y + getBounds().height
        }
        else {
            centerX -= (getBounds().height / 2) * dx / dy,
            centerY = getBounds().y
        }
    }
    return{
        x: centerX,
        y: centerY
    }
  }

  draw(){
    const connectedLine = getConnectionPoint()
    const panel = document.getElementById('graphpanel')
    const line = document.createElementNS('http://www.w3.org/2000/svg', 'line')
    line.setAttribute('x1', connectedLine.x)
    line.setAttribute('y1', connectedLine.y)
    line.setAttribute('x2', connectedLine.x)
    line.setAttribute('y2', connectedLine.y)
    line.setAttribute('stroke', 'black')
  //  if(lineStyle === 1){
    //line.setAttribute('stroke-dasharray', 3)}
    panel.appendChild(line)
  }
  getStart(){
    return this.start
  }
  getEnd(){
    return this.end
  }
}

class createCurveEdge extends RectangleNode{
  constructor(s,e){
    super(s,e)
  }


   getBounds(){
     return {
      x: this.start.getBounds().x + this.start.getBounds().width/2,
      y: this.start.getBounds().y + this.start.getBounds().height/2
     }
   }
   getStart(){
     return this.start
   }
   getEnd(){
     return this.end
   }
   getConnectionPoints(){
       let startBounds = start.getBounds()
       let endBounds = end.getBounds()
       let startCenter = { x: startBounds.x + startBounds.width / 2, y: startBounds.y + startBounds.height}
       let endCenter = { x: endBounds.x + endBounds.width / 2, y: endBounds.y + endBounds.height}
       let point1 = start.getConnectionPoint(endCenter)
       let point2 = end.getConnectionPoint(startCenter)
       return {
         x1: point1.x,
          x2: point2.x,
          y1: point1.y,
          y2: point2.y
            }
   }
   draw(){
     const connectedLine = getConnectionPoint()
     const panel = document.getElementById('graphpanel')
     const lineV = document.createElementNS('http://www.w3.org/2000/svg', 'line')
     const lineH = document.createElementNS('http://www.w3.org/2000/svg', 'line')
     lineV.setAttribute('x1', connectedLine.x)
    lineV.setAttribute('y1', connectedLine.y)
      lineV.setAttribute('x2', connectedLine.x)
      lineV.setAttribute('y2', connectedLine.y)

      lineH.setAttribute('x1', connectedLine.x)
      lineH.setAttribute('y1', connectedLine.y)
      lineH.setAttribute('x2', connectedLine.x)
    lineH.setAttribute('y2', connectedLine.y)


              lineV.setAttribute('stroke', 'black')
              lineH.setAttribute('stroke', 'black')
    // if(lineStyle === 1){
     //line.setAttribute('stroke-dasharray', 3)}
     panel.appendChild(lineH)
     panel.appendChild(lineV)
   }
   clone(){
   return new createCurveEdge()
    }
   }



//
// function createLineEdge(){
//     let start = undefined;
//     let end = undefined
//     let lineStlye = undefined
//     return {
//         connect: (s, e) => {
//             start = s;
//             end = e
//         },
//         setLineStyle:(s) =>{
//           lineStyle = s
//         },
//         getType:() => {
//           return "LineEdge"
//         },
//
//         getLineStyle: () =>{
//           return lineStyle
//         },
//         getStart: () => {
//             return {
//                 start
//             }
//         },
//         getEnd: () => {
//             return {
//                 end
//             }
//         },
//
//         getConnectionPoints: () => {
//             let startBounds = start.getBounds()
//             let endBounds = end.getBounds()
//             let startCenter = { x: startBounds.x + startBounds.width / 2, y: startBounds.y + startBounds.height}
//             let endCenter = { x: endBounds.x + endBounds.width / 2, y: endBounds.y + endBounds.height}
//             let point1 = start.getConnectionPoint(endCenter)
//             let point2 = end.getConnectionPoint(startCenter)
//             return {
//                 x1: point1.x,
//                 x2: point2.x,
//                 y1: point1.y,
//                 y2: point2.y
//             }
//         },
//
//         draw: () => {
//             const connectedLine = getConnectionPoint()
//             const panel = document.getElementById('graphpanel')
//             const line = document.createElementNS('http://www.w3.org/2000/svg', 'line')
//             line.setAttribute('x1', connectedLine.x)
//             line.setAttribute('y1', connectedLine.y)
//             line.setAttribute('x2', connectedLine.x)
//             line.setAttribute('y2', connectedLine.y)
//             line.setAttribute('stroke', 'black')
//             if(lineStyle === 1){
//             line.setAttribute('stroke-dasharray', 3)
//           }
//             panel.appendChild(line)
//         },
//         clone: () => {
//           return new createLineEdge()
//         }
//     }
//   }
//
//     function createCurveEdge()
//     {
//       let start = undefined;
//       let end = undefined
//       let startPoint = undefined
//       let endPoint = undefined
//       let lineStlye = undefined
//       return{
//         connect: (s, e) => {
//             start = s;
//             end = e
//         },
//         setLineStyle:(s) =>{
//           lineStyle = s
//         },
//         getLineStyle: () =>{
//           return lineStyle
//         },
//         getStart: () => {
//             return {
//                 start
//             }
//         },
//
//         getEnd: () => {
//             return {
//                 end
//             }
//         },
//         getConnectionPoints: () => {
//             let startBounds = start.getBounds()
//             let endBounds = end.getBounds()
//             let startCenter = { x: startBounds.x + startBounds.width / 2, y: startBounds.y + startBounds.height}
//             let endCenter = { x: endBounds.x + endBounds.width / 2, y: endBounds.y + endBounds.height}
//             let point1 = start.getConnectionPoint(endCenter)
//             let point2 = end.getConnectionPoint(startCenter)
//             return {
//                 x1: point1.x,
//                 x2: point2.x,
//                 y1: point1.y,
//                 y2: point2.y
//             }
//         },
//         draw: () => {
//             let p1 = {
//               x: start.getBounds().x,
//               y: start.getBounds().y
//             }
//             let p2 = {
//               x: end.getBounds().x,
//               y: end.getBounds().y
//             }
//             startPoint = start.getConnectionPoint(p2)
//             endPoint = end.getConnectionPoint(p1)
//             startPoint = start.getConnectionPoint(endPoint)
//             endPoint = end.getConnectionPoint(startPoint)
//
//
//             const panel = document.getElementById('graphpanel')
//             const lineV = document.createElementNS('http://www.w3.org/2000/svg', 'line')
//             const lineH = document.createElementNS('http://www.w3.org/2000/svg', 'line')
//
//
//             lineV.setAttribute('x1', startPoint.x)
//             lineV.setAttribute('y1', startPoint.y)
//             lineV.setAttribute('x2', startPoint.x)
//             lineV.setAttribute('y2', endPoint.y)
//
//             lineH.setAttribute('x1', endPoint.x)
//             lineH.setAttribute('y1', endPoint.y)
//             lineH.setAttribute('x2', startPoint.x)
//             lineH.setAttribute('y2', endPoint.y)
//
//
//             lineV.setAttribute('stroke', 'black')
//             lineH.setAttribute('stroke', 'black')
//
//             if(lineStyle === 1){
//             lineV.setAttribute('stroke-dasharray', 3)
//             lineH.setAttribute('stroke-dasharray', 3)
//           }
//             panel.appendChild(lineH)
//             panel.appendChild(lineV)
//       },
//
//       clone: () => {
//         return new createCurveEdge()
//       }
//
// }
// }
