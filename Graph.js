class Graph {
  constructor() {
    this.nodes = []
    this.edges = []
    this.psheet = [] //array of 1 
  }
  add(n) {
    this.nodes.push(n)
  }
  addp(ps) {
    this.psheet.push(ps)
    console.log(ps)//prints out propertysheet
  }
  findNode(p) {
    for (let i = this.nodes.length - 1; i >= 0; i--) {
      const n = this.nodes[i]
      if (n.contains(p)) return n
    }
    return undefined
  }
  findProp(point) { //takes a point where the mouse is as the parameter
    for (let i = 0; i < this.psheet.length - 1; i++) { //looks through propertysheet array(this will be 1 if there is a property sheet)
      const ps = this.psheet[i] //ps = propertysheet
      if (ps.contains(point)) 
	{
		return ps; //if ps contains the point
	}
    }
    return undefined //returns undefine if no property sheet is found
  }
  draw() {
    for (const n of this.nodes) {
      n.draw()
    }
    for (const ps of this.psheet) { //draws propertysheet (works)
      ps.draw()
    }    
  }
  removeProp(){ //removes propertysheet
    this.psheet.pop() // pop is removing last element in array
  }
}
