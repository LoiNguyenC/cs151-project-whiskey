'use strict'



function createCircleNode (x, y, size, color) {
  return {
    getBounds: () => {
      return {
        x: x,
        y: y,
        width: size,
        height: size
      }
    },
    contains: p => {
      return (x + size / 2 - p.x) ** 2 + (y + size / 2 - p.y) ** 2 <= size ** 2 / 4
    },
    translate: (dx, dy) => {
      x += dx
      y += dy
    },
    draw: () => {
      const panel = document.getElementById('graphpanel')
      const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle')
      circle.setAttribute('cx', x + size / 2)
      circle.setAttribute('cy', y + size / 2)
      circle.setAttribute('r', size / 2)
      circle.setAttribute('fill', color)
      panel.appendChild(circle)
    },
    getConnectionPoint: p =>{
      let centerX = x + this.size /2
      let centerY = y + this.size /2
      let dx = p.x - centerX
      let dy = p.y - centerY
      let distance = Math.sqrt(dx*dx + dy * dy)
      if(distance === 0)
      return p
      else{
        return{ x: centerX + dx * (size / 2) / distance,
           y: centerY + dy * (size / 2) / distance }
      }
    }
  }
}

function drawGrabber(x,y){
const size = 5;
const panel = document.getElementById('graphpanel')
const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
rect.setAttribute
rect.setAttribute('x', x - size / 2)
rect.setAttribute('y', y - size / 2)
rect.setAttribute('width', size )
rect.setAttribute('height', size )
rect.setAttribute('fill', 'black')
panel.appendChild(rect)
}

class Graph {
  constructor() {
    this.nodes = []
    this.edges = []
  }
  addNodes(n) {
    this.nodes.push(n)
  }
  addEdges(e){
    edges.push(e)
  }
  findNode(p) {
    for (let i = this.nodes.length - 1; i >= 0; i--) {
      const n = this.nodes[i]
      if (n.contains(p)) return n
    }
    return undefined
  }
  findEdge(p){
    for (let i = edges.length - 1; i >= 0; i--) {
         const e = this.edges[i]
         if (e.contains(p))
	   return e
      }
      return undefined
  }


  draw() {
    for (const n of this.nodes) {
      n.draw()
    }
    for (const e of this.edges) {
      e.draw()
    }
  }
  connect(e, p1, p2)
  {
    const n1 = this.findNode(p1)
    const n2 = this.findNode(p2)
    if (n1 !== undefined && n2 !== undefined) {
      e.connect(n1, n2)
      this.edges.push(e)
      return true
    }
    return false
  }
}


function center(rect){
  return { x: rect.x + rect.width / 2, y: rect.y + rect.height / 2 }
}

function createLineEdge(){
  let start = undefined
  let end = undefined
  let lineStyle = undefined
  let centerPointOne = undefined
  let centerPointTwo = undefined
  return{

    getType: () => {
      return 'Edge'
    },
    connect: (s, e) => {
      start = s
      end = e
    },
    draw: () => {
      const panel = document.getElementById('graphpanel')

      const line = document.createElementNS('http://www.w3.org/2000/svg', 'line')
      let p1 = {
              x: from.getBounds().x,
              y: from.getBounds().y
            }
            let p2 = {
              x: to.getBounds().x,
              y: to.getBounds().y
            }
            fromPoint = from.getConnectionPoint(p2)
            toPoint = to.getConnectionPoint(p1)
            fromPoint = from.getConnectionPoint(toPoint)
            toPoint = to.getConnectionPoint(fromPoint)
            line.setAttribute('id', 'edge')
            line.setAttribute('x1', fromPoint.x)
            line.setAttribute('y1', fromPoint.y)
            line.setAttribute('x2', toPoint.x)
            line.setAttribute('y2', toPoint.y)
            line.setAttribute("stroke", "black")
            panel.appendChild(line)
    },
    clone: () => {
      return createLineEdge()
    }
  }
}

document.addEventListener('DOMContentLoaded', function () {
  const graph = new Graph()
  const n1 = createCircleNode(10, 10, 20, 'black')
  const n2 = createCircleNode(30, 30, 20, 'blue')
  graph.addNodes(n1)
  graph.addNodes(n2)
  graph.draw()
  const e = createLineEdge()
  graph.connect(e, { x: 20, y: 20 }, { x: 40, y: 40 })

  function mouseLocation(event) {
   const rect = panel.getBoundingClientRect();
   return {
     x: event.clientX - rect.left,
     y: event.clientY - rect.top,
   }
 }

 let selected = undefined

 function repaint() {
    panel.innerHTML = ''
    graph.draw()
    if (selected !== undefined) {
      const bounds = selected.getBounds()
      drawGrabber(bounds.x, bounds.y)
      drawGrabber(bounds.x + bounds.width, bounds.y)
      drawGrabber(bounds.x, bounds.y + bounds.height)
      drawGrabber(bounds.x + bounds.width, bounds.y + bounds.height)
    }
  }
  let dragStartPoint = undefined
  let dragStartBounds = undefined

   const panel = document.getElementById('graphpanel')
     panel.addEventListener('mousedown', event => {
       let mousePoint = mouseLocation(event)
       dragStartPoint = mousePoint
        selected = graph.findNode(mousePoint)
       if (selected) {
         dragStartBounds = selected.getBounds()}
         repaint()
     })

     panel.addEventListener('mousemove', event => {
       let mousePoint = mouseLocation(event)
       if(dragStartBounds !== null) {
         const bounds = selected.getBounds()
         selected.translate(
           dragStartBounds.x - bounds.x
           + mousePoint.x - dragStartPoint.x,
           dragStartBounds.y - bounds.y
           + mousePoint.y - dragStartPoint.y)
       }
       repaint()
     })
     panel.addEventListener('mouseup', event => {
       let mousePoint = mouseLocation(event)
       dragStartPoint = undefined
       dragStartBounds = undefined
       repaint()
     })


})
