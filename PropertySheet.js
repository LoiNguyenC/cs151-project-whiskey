'use strict'
class PropertySheet {
  constructor(n,g) {
    this.x = 500
    this.y = 100
    this.width = 250 
    this.height = 200
    this.currentNode = n
    this.properties = []
    this.graph = g

  }
  addProperties(o) {
    for (let i = 0;i< o.length;i++) {
        this.properties.push(o[i]);
    }
  }
  getBounds(){
    return {
        x: this.x, 
        y: this.y,  
        width: this.width, 
        height: this.height, 
    } 
  }
  translate(dx, dy){
        this.x += dx
        this.y += dy
  }
  contains(p){ //checks if point is within the propertysheet
    return (p.x >= this.x && p.x <= this.x + this.width) && (p.y >= this.y && p.y <= this.y + this.height)
  }
  draw() {
  	let height = 200
  	let width = 250
        const panel = document.getElementById('graphpanel')

        let propSheet = document.createElementNS('http://www.w3.org/2000/svg', 'g') //makes a group, then add rectangle,text,textbox to it
	//creates the rect for propertysheet
	let propSheetRect = document.createElementNS('http://www.w3.org/2000/svg','rect')	//creates the rect for propertysheet
	propSheetRect.setAttribute('x', this.x)
        propSheetRect.setAttribute('y', this.y)
        propSheetRect.setAttribute('width', this.width)
        propSheetRect.setAttribute('height', this.height)
        propSheetRect.setAttribute('fill', 'white')
        propSheetRect.setAttribute('stroke', 'black')
  	propSheet.appendChild(propSheetRect)

	//properties name on top
	const propertyName = document.createElementNS('http://www.w3.org/2000/svg','text')
	propertyName.setAttribute('x',this.x + 10) //x location of text
	propertyName.setAttribute('y',this.y + 15 ) //y location of text
	propertyName.setAttribute('font-size', '10') //font size = 10
	let valueOfText = 'Properties'
	const textNode = document.createTextNode(valueOfText)
	propertyName.appendChild(textNode)
  	propSheet.appendChild(propertyName)
	//properties of node


	for(let i = 0; i < this.properties.length;i++){ //iterates through list of properties
	let propertyName2 = document.createElementNS('http://www.w3.org/2000/svg','text')
	propertyName2.setAttribute('x',this.x + 35) //x location of text
	propertyName2.setAttribute('y',this.y + 50 + 50*i ) //y location of text
	propertyName2.setAttribute('font-size', '10') //font size = 10
	let valueOfText2 = this.properties[i].name
	const textNode2 = document.createTextNode(valueOfText2)
	propertyName2.appendChild(textNode2)
	
	//https://stackoverflow.com/questions/29295322/javascript-to-create-a-textarea-with-foreign-object-in-svg-not-working
	let fo = document.createElementNS('http://www.w3.org/2000/svg','foreignObject')
	fo.setAttribute('id','ta')
	fo.setAttribute('x',this.x + 85)
	fo.setAttribute('y',this.y + 35 + 45*i)
	fo.setAttribute('width',160)
	fo.setAttribute('height',50) 

	let textA = document.createElement('textarea')
	textA.id = 'ta' 
	textA.textContent += this.properties[i].value
	textA.rows = 2
	textA.cols = 20
	
	textA.addEventListener('keydown', event => {
	let newText = textA.textContent

	if(i === 0)
	{
	let oldText = this.currentNode.getText1()
	newText = oldText + event.key
	this.currentNode.setText1(newText)
	this.graph.draw()
	} else if (i === 1) {
	let oldText = this.currentNode.getText2()
	newText = oldText + event.key
	this.currentNode.setText2(newText)
	this.graph.draw()
	}else if (i === 2){
	let oldText = this.currentNode.getText3()
	newText = oldText + event.key
	this.currentNode.setText3(newText)
	this.graph.draw()
	}


	})

	fo.appendChild(textA)
	propSheet.appendChild(propertyName2)// put the property name like name or methods
	propSheet.appendChild(fo)
	}

	

	//creates button, updates node, removes propertysheet from graph,
	let fo2 = document.createElementNS('http://www.w3.org/2000/svg','foreignObject')
	fo2.setAttribute('id','btn')
	fo2.setAttribute('x',this.x + 90)
	fo2.setAttribute('y',this.y + 175)
	fo2.setAttribute('width',70)
	fo2.setAttribute('height',50)

	const okButton = document.createElement('button')
	okButton.setAttribute('type','button') //type is button
   	okButton.innerHTML = 'OK' //buttons says Ok

   	okButton.addEventListener('click', event => { //this event work,removes property sheet and update classnode
		let ns1 = this.currentNode.getText1()
		let ns2 = this.currentNode.getText2()
		let ns3 = this.currentNode.getText3()
		this.currentNode.setText1(ns1)
		this.currentNode.setText2(ns2)
		this.currentNode.setText3(ns3)
		this.graph.draw() //resize the node
		this.graph.removeProp() //remove property sheet
   		panel.removeChild(propSheet)
   	})

	fo2.appendChild(okButton)
	propSheet.appendChild(fo2)

  	panel.appendChild(propSheet) //appends group named propSheet to the panel
  	
  
  
  
  
  
  }
 }


